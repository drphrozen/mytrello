import 'dart:async';
import 'dart:html';
import 'package:json_object/json_object.dart';
import 'dart:mirrors';


class AuthOptions {
  final String type;
  final bool persist;
  final bool interactive;
  final String expiration;
  final AuthScope scope;
  final String name;
  
  AuthOptions({
    this.type: "redirect",
    this.persist: true,
    this.interactive: true,
    this.scope: const AuthScope(),
    /// "1hour", "1day", "30days" or "never"
    this.expiration: "30days",
    this.name: null
  });
}

class AuthScope {
  final bool read;
  final bool write;
  final bool account;
  const AuthScope({this.read: true, this.write: false, this.account: false});
}

class Trello {
  String key;
  String token;
  final String version; 
  final String apiEndpoint;
  final String authEndpoint;
  String _baseUrl;
  Stream<String> onAuthorized;

  Trello({ this.key, this.token, this.apiEndpoint: 'https://api.trello.com', this.authEndpoint: 'https://trello.com' , this.version: '1' })
  {
    _baseUrl = "$apiEndpoint/$version/";
    var authorizedController = new StreamController();
    onAuthorized = authorizedController.stream;
    window.onMessage.listen((MessageEvent event) {
      if(event.origin != authEndpoint)
        return;
      var source = event.source as WindowBase;
      if(source != null)
        source.close();
      var data = event.data as String;
      var token = (data != null && data.length > 4)
         ? data
         : null;
      authorizedController.add(token);
    });
  }
  
  bool authorized() { return token != null; }
  
  void deauthorize() {
    token = null;
    writeStorage("token", token);
  }
  
  void writeStorage(String key, String value) {
    window.localStorage[key] = value;
  }
  
  String readStorage(String key) {
    return window.localStorage[key];
  }

  final RegExp regexToken = new RegExp(r"[&#]?token=([0-9a-f]{64})");
  
  String asQuery(Map<String, String> map) {
    return map.keys.where((key) => map[key] != null).map((key) => Uri.encodeQueryComponent(key) + "=" + Uri.encodeQueryComponent(map[key])).join("&");
  }
  
  String authorizeURL({Map<String, String> map: null}) {
    if(key == null) {
      key = this.key;
    }
    if(map == null) {
      map = new Map<String, String>();
    }
    map.putIfAbsent('response_type', () => 'token');
    map.putIfAbsent('key', () => this.key);
    
    return "$authEndpoint/$version/authorize?" + asQuery(map);
  }

  /// expiration: "1hour", "1day", "30days" or "never"
  Future<String> authorize(AuthOptions options) {
    var completer = new Completer();
      void persistToken() {
        if (options.persist && token != null) {
          writeStorage("token", token);
        }
      }
      if (options.persist && token == null)
        token = readStorage("token");
      
      if (token == null) {
        var match = regexToken.firstMatch(window.location.hash);
        if(match != null)
          token = match[1];
      }

      if (authorized()) {
        persistToken();
        window.location.hash = window.location.hash.replaceFirst(regexToken, "");
        completer.complete(null);
        return completer.future;
      }
      // If we aren't in interactive mode, and we didn't get the token from
      // storage or from the hash, then we error out here
      if(!options.interactive) {
        completer.completeError("Not interactive and missing token!");
        return completer.future;
      }
      
      var list = new List<String>();
      
      if(options.scope.account)
        list.add("account");
      if(options.scope.read)
        list.add("read");
      if(options.scope.write)
        list.add("write");
        
      var scope = list.join(",");

      switch(options.type) {
        case "popup":
          onAuthorized.first.then((token) {
            if(authorized()) {
              persistToken();
              completer.complete(token);
            } else {
              completer.completeError("Not authorized!");
            }
          });
          
          var width = 420;
          var height = 470;
          var left = window.screenX + (window.innerWidth - width) / 2;
          var top = window.screenY + (window.innerHeight - height) / 2;

          var originMatch = new RegExp(r'^[a-z]+:\/\/[^\/]*').firstMatch(window.location.href);
          var origin = originMatch != null
            ? originMatch[0]
            : null;
          var url = authorizeURL(map: { 'return_url': origin, 'callback_method': "postMessage", 'scope': scope, 'expiration': options.expiration, 'name': options.name});
          window.open(url, "trello", "width=$width,height=$height,left=$left,top=$top");
          break;
        default:
          //We're leaving the current page now; but the user should be calling .authorize({ interactive: false })
          //on page load
          var url = authorizeURL(map: { 'redirect_uri': window.location.href, 'callback_method': "fragment", 'scope': scope, 'expiration': options.expiration, 'name': options.name});
          window.location.href = url;
          break;
      }
      return completer.future;
    }
    
  Future<JsonObject> get(String path) {
    return rest('GET', path);
  }
  
  Future<JsonObject> put(String path) {
    return rest('PUT', path);
  }
  
  Future<JsonObject> post(String path) {
    return rest('POST', path);
  }
  
  Future<JsonObject> delete(String path) {
    return rest('DELETE', path);
  }
  
  Future<JsonObject> rest(String method, String path, {Map<String, String> params: null}) {
    var data = new Map<String, String>();
    if(key != null) {
      data["key"] = key;
    }
    if(token != null) {
      data["token"] = token;
    }
    path = path.replaceFirst(new RegExp(r'^\/*'), "");
    if(params != null) {
      params.keys.forEach((key) {
        data[key] = params[key]; 
      });
    }
    var query = asQuery(data);
    return HttpRequest.request("$_baseUrl$path?$query", method: method).then((x) => new JsonObject.fromJsonString(x.responseText));
  }

  Future<JsonObject> actions(String id) {
    return rest('GET', 'actions/$id');
  }

  Future<JsonObject> cards(String id) {
    return rest('GET', 'cards/$id');
  }

  Future<JsonObject> checklists(String id) {
    return rest('GET', 'checklists/$id');
  }

  Future<JsonObject> boards(String id) {
    return rest('GET', 'boards/$id');
  }

  Future<JsonObject> lists(String id) {
    return rest('GET', 'lists/$id');
  }

  Future<Member> members(String id) {
    return rest('GET', 'members/$id').then((x) => new Member(x));
  }

  Future<JsonObject> organizations(String id) {
    return rest('GET', 'organizations/$id');
  }
}

class Member extends MirroredJsonObject {
  Member(JsonObject jsonObject) : super(jsonObject);

  String avatarHash;
  String avatarSource;
  String bio;
  String bioData;
  bool confirmed;
  String email;
  String fullName;
  String gravatarHash;
  String id;

  List<String> idBoards;
  String idBoardsPinned;
  List<String> idOrganizations;
  List<String> idPremOrgsAdmin;
  String initials;
  String loginTypes;
  String memberType;
  String newEmail;
  List<String> oneTimeMessagesDismissed;
  JsonObject prefs;
  List<String> premiumFeatures;
  List<String> products;
  String status;
  List<String> trophies;
  String uploadedAvatarHash;
  String url;
  String username;
}

class MirroredJsonObject {
  MirroredJsonObject(JsonObject jsonObject) {
    var cm = reflectClass(this.runtimeType);
    var om = reflect(this);
    for (VariableMirror vm in cm.declarations.values.where((x) => x is VariableMirror)) {
      var field = MirrorSystem.getName(vm.simpleName);
      om.setField(vm.simpleName, jsonObject[field]);
    }
  }
}